from engine import Engine
import numpy as np

input_layer = 784 # 28x28
hidden_layers = (400, 200, 100, 50)
output_layer = 11 # 1 2 3 4 5 6 7 8 9 0 null

recognizer = Engine(input_layer, hidden_layers, output_layer)
recognizer.randomizeWeights()

i = np.random.rand(1, 784)
out = recognizer.predict(i)
print(out)