import numpy as np

#TODO: add backpropagation

class Engine:

    def __init__(self, i, h, o):
        self.input_count = i
        self.hidden_count = h
        self.hidden_layer_count = len(h)
        self.output_count = o
        self.weights = []
        self.bias = []
        self.weights_initialized = False

    # Randomize weights and bias from (-1, 1)
    def randomizeWeights(self):
        self.weights.append((np.random.rand(self.input_count, self.hidden_count[0])-0.5)*2)
        self.bias.append((np.random.rand(1, self.hidden_count[0])-0.5)*2)
        for x in range(0, self.hidden_layer_count-1):
            self.weights.append((np.random.rand(self.hidden_count[x], self.hidden_count[x+1])-0.5)*2)
            self.bias.append((np.random.rand(1, self.hidden_count[x+1])-0.5)*2)
        self.weights.append((np.random.rand(self.hidden_count[self.hidden_layer_count-1], self.output_count)-0.5)*2)
        self.bias.append((np.random.rand(1, self.output_count)-0.5)*2)
        self.weights_initialized = True

    def predict(self, inp):
        if self.weights_initialized:
            hidden = self.ReLu((np.matmul(inp, self.weights[0]) + self.bias[0])[0])
            for x in range(1, self.hidden_layer_count):
                hidden = self.ReLu((np.matmul(hidden, self.weights[x]) + self.bias[x])[0])
            output = self.softmax((np.matmul(hidden, self.weights[self.hidden_layer_count]) + self.bias[len(self.bias)-1])[0])
            return output
        else:
            self.errorWeightsNotInit("predict")

    def train(self, inp, labels):
        if self.weights_initialized:
            hidden = []
            hidden.append(self.ReLu((np.matmul(inp, self.weights[0]) + self.bias[0])[0]))
            for x in range(1, self.hidden_layer_count):
                hidden.append(self.ReLu((np.matmul(hidden[len(hidden)-1], self.weights[x]) + self.bias[x])[0]))
            result = self.softmax((np.matmul(hidden[len(hidden)-1], self.weights[len(self.weights)-1]) + self.bias[len(self.bias)-1])[0])
            
            

        else:
            self.errorWeightsNotInit("train")

    def ReLu(self, array):
        result = []
        for x in array:
            if x > 0:
                result.append(x)
            else:
                result.append(0)
        return result

    def softmax(self, array):
        sum = np.sum(array)
        result = []
        for x in array:
            result.append(x/sum)
        return result

    def errorWeightsNotInit(self, func):
        print("Function: "+func)
        print("Weights not initialized")
        print("Exiting program")
        exit()